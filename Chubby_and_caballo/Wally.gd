extends MeshInstance

var locmat

func _process(delta):
	var diffc = Color( rand_range(0,1), rand_range(0,1), rand_range(0,1) )
	#var mesh = get_mesh()
	#var material_on_surface_zero = mesh.surface_get_material(0)
	#material_on_surface_zero.set_parameter(FixedMaterial.PARAM_EMISSION, diffc)
	locmat.set_parameter(FixedMaterial.PARAM_EMISSION, diffc)

func _ready():
	var mesh = get_mesh()
	var material_on_surface_zero = mesh.surface_get_material(0)
	locmat = material_on_surface_zero.duplicate( false )
	set_material_override( locmat )
	set_process( true )
	pass
